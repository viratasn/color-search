//React
import { useState, useEffect } from "react";
//Styles
import styles from "../../styles/Home.module.css";
//Components
import ColorCardComponent from "./cards/ColorCardComponent";
//External services
import axios from "axios";
//Hooks
import { useDebounce } from "use-debounce";
//end points
import { COLOR_SEARCH_API } from "../services/apiEndPoints";

const ColorSearchLayout = () => {
  /**
   * State for handling the input entered by the user
   */
  const [colorValue, setColorValue] = useState("");

  /**
   * Loading state while fetching the data from api
   */
  const [loading, setLoading] = useState(false);

  /**
   * Using a debouncer to let the user finish his/her typing
   */
  const [debouncedColor] = useDebounce(colorValue, 1000);

  /**
   * State for storing the colors fetched from the api
   */
  const [colors, setColors] = useState([]);

  /**
   * Function for handling the input value entered by the user
   * @param {*} event
   */
  const handleColorChange = (event) => {
    setColorValue(event.target.value);
  };

  const where = encodeURIComponent(
    JSON.stringify({
      name: {
        $regex: colorValue,
      },
    })
  );

  /**
   * Effect for fetching the data from the api and listens to the text change which user enters
   */
  useEffect(() => {
    setLoading(true);
    axios
      .get(`${COLOR_SEARCH_API}?count=1&limit=10000&order=name&where=${where}`, {
        headers: {
          "X-Parse-Application-Id": "vei5uu7QWv5PsN3vS33pfc7MPeOPeZkrOcP24yNX", 
          "X-Parse-Master-Key": "aImLE6lX86EFpea2nDjq9123qJnG0hxke416U7Je", 
        },
      })
      .then((res) => {
        setColors(res.data.results);
        setLoading(false);
      });
    return () => {};
  }, [debouncedColor]);

  /**
   * Function to render color grid
   * @returns a grid of colors
   */
  const renderColorCards = () => {
    if (!loading) {
      if (colors.length > 0) {
        return (
          <div className={styles.colorCardRow}>
            {colors.map((color, index) => {
              return (
                <div className={styles.colorCardColumn} key={index}>
                  <ColorCardComponent color={color} />
                </div>
              );
            })}
          </div>
        );
      } else {
        return <p className="text-center">No colors found please search with some different text....</p>;
      }
    } else {
      return <p className="text-center">Searching....</p>;
    }
  };

  return (
    <div className={styles.pageWrapper}>
      <p className="text-center">
        <input placeholder="Enter a color name" className={styles.colorSearchBar} type="text" value={colorValue} onChange={handleColorChange}></input>
      </p>
      {renderColorCards()}
    </div>
  );
};

export default ColorSearchLayout;
