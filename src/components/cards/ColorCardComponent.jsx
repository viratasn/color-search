import styles from "../../../styles/Home.module.css";
function ColorCardComponent({ color }) {
  console.log(color);
  return (
    <>
      <div className={styles.colorCardHeader} >
          <span className="text-center">{ color.hexCode }</span>
          <span className="text-center">{ color.name }</span>
      </div>
      <div style={{ background: color.hexCode }} className={styles.colorCard}></div>
    </>
  );
}

export default ColorCardComponent;
