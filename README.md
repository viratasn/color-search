## To run this project

First, run the development server:

```bash
1. npm install
# or
yarn install

2. npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## External APIs Used

1. [Colors](https://parseapi.back4app.com/classes/Color)

## Libraries Used

1. [use-debounce](https://www.npmjs.com/package/use-debounce) - To wait for the user to finish his typing while searching
2. [axios](https://www.npmjs.com/package/axios) - To make network requests
## Deployment Link

[Live Preview](https://color-search-three.vercel.app/)
